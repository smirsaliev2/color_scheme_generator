const colorSchemeModes = ['monochrome', 'monochrome-dark', 'monochrome-light', 'analogic', 'complement', 
    'analogic-complement', 'triad', 'quad']
const colorCount = 5;

renderColorScheme({hex: 000000, mode: 'monochrome'})

// Adding modes to <select>
document.querySelector('.scheme-modes-select').innerHTML = colorSchemeModes.map(mode => 
    `<option>${mode}</option>`).join('')

document.querySelector('#get-scheme-form').addEventListener('submit', submitForm)

function submitForm(e) {
    e.preventDefault()
    const hex = document.querySelector('.color-input').value.substring(1) //getting rid of [0] cos api doesn't need '#'
    const mode = document.querySelector('.scheme-modes-select').value
    renderColorScheme({
        hex,
        mode
    })
}

function renderColorScheme(params) {
    endpoint = `/scheme?hex=${params.hex}&mode=${params.mode}&count=${colorCount}`
    fetch(`https://www.thecolorapi.com${endpoint}`)
        .then(response => response.json())
        .then(data => {
            let colorsHtml = ''
            for (let color of data.colors) {
                colorsHtml += `
                    <div class="color-container">
                        <div class="color" style="background-color:${color.hex.value};"></div>
                        <div class="color-code">${color.hex.value}</div>
                        <div class="color-name">${color.name.value}</div>
                    </div>`
            }

            document.querySelector('.color-palette').innerHTML = colorsHtml;

            document.querySelectorAll('.color-code').forEach(function(node) {
                node.addEventListener('click', copyToClipboard)  
                
            })
        })
}

function copyToClipboard(event) {
    const textContent = event.target.textContent
    navigator.clipboard.writeText(textContent)
        .then(
            () => {showSnackbar(textContent)},
            () => {console.log('gg')}
        );
}

function showSnackbar(text) {
    const x = document.getElementById("snackbar")
    x.textContent = `Hex copied: ${text}`
    x.className = "show"
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 1000)
}
